import moment from "moment";

export function formatDate(dateStr: string) {
  const date = moment(dateStr, "MM-DD-YYYY");

  if (!date.isValid()) {
    throw new Error("dateStr is invalid. Please check it is a valid date with format MM-DD-YYYY");
  }

  return date.format("MMM D, YYYY");
}

export function formatDates(startDateStr: string, endDateStr: string) {
  const startDate = formatDate(startDateStr);
  const endDate = formatDate(endDateStr);

  if (startDate === endDate) {
    return startDate;
  }

  return startDate.concat(" - ", endDate);
}
