import { shallow, ShallowWrapper } from "enzyme";
import React from "react";

import { Conference } from "../../../src/components/Conference";
import { ConferenceFactory } from "../../factories/Conference";

it("shows important conference-related information in its expected element", () => {
  const conference = ConferenceFactory({
    date: {
      end: "11-16-2018",
      start: "11-11-2018"
    },
    location: {
      city: "Potsdam",
      country: "Germany",
      emoji_country: "🇩🇪"
    },
    name: "Agile Testing Days"
  });

  const subject = shallow(<Conference {...conference} />);

  expect(subject.find(".Conference__Title").text()).toEqual("Conference Agile Testing Days");
  expect(subject.find(".Conference__Schedule").text()).toEqual("Nov 11, 2018 - Nov 16, 2018");
  expect(subject.find(".Conference__Location").text()).toEqual("Potsdam, Germany");
  expect(subject.find(".Conference__CoC").exists()).toBe(true);
  expect(subject.find(".Conference__C4P").exists()).toBe(true);
});

describe("handling conference title", () => {
  let subject: ShallowWrapper;

  beforeEach(() => {
    const conference = ConferenceFactory({
      name: "Agile Testing Days",
      url: "https://agiletestingdays.com"
    });

    const wrapper = shallow(<Conference {...conference} />);
    subject = wrapper.find(".Conference__Title");
  });

  it("includes a link to the conference's website", () => {
    expect(
      subject.containsMatchingElement(<a href="https://agiletestingdays.com">Agile Testing Days</a>)
    ).toBe(true);
  });

  it("sets HTML attribute to open link in a new tab", () => {
    expect(subject.containsMatchingElement(<a target="blank">Agile Testing Days</a>)).toBe(true);
  });

  it("sets proper CSS class to style the link", () => {
    expect(
      subject.containsMatchingElement(<a className="Conference__Link">Agile Testing Days</a>)
    ).toBe(true);
  });
});

describe("handling CoC", () => {
  it("is not shown if the conference does not have one", () => {
    const conference = ConferenceFactory({ hasCoc: false });
    const subject = shallow(<Conference {...conference} />);

    expect(subject.find(".Conference__CoC").exists()).toBe(false);
  });
});

describe("handling C4P", () => {
  it("is not shown if the conference does not have one", () => {
    const conference = ConferenceFactory({ hasCfp: false });
    const subject = shallow(<Conference {...conference} />);

    expect(subject.find(".Conference__C4P").exists()).toBe(false);
  });
});

describe("handling conclusion", () => {
  it("applies a special class name when the conference has concluded", () => {
    const conference = ConferenceFactory({ isConcluded: true });
    const subject = shallow(<Conference {...conference} />);

    expect(subject.hasClass("Conference--isConcluded")).toBe(true);
  });

  it("does not apply a special class name when the conference has concluded", () => {
    const conference = ConferenceFactory({ isConcluded: false });
    const subject = shallow(<Conference {...conference} />);

    expect(subject.hasClass("Conference--isConcluded")).toBe(false);
  });
});
