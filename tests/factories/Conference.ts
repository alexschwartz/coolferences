import { Conference } from "../../src/entities/Conference";
import { ConferenceFactoryBuilder } from "../../src/entities/factories/ConferenceFactoryBuilder";
import { ConferenceJsonFactory } from "./ConferenceJson";

export function ConferenceFactory(overrides?: Partial<Conference>) {
  const factory = ConferenceFactoryBuilder({ today: new Date() });
  const defaults = factory(ConferenceJsonFactory());

  return { ...defaults, ...overrides };
}
