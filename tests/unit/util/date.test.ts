import { formatDate, formatDates } from "../../../src/util/date";

describe("#formatDate", () => {
  it("formats a date string into a readable format", () => {
    const result = formatDate("01-02-2018");

    expect(result).toEqual("Jan 2, 2018");
  });

  it("throws an exception if it builds an invalid date", () => {
    expect(() => formatDate("21-02-2018")).toThrowError();
  });
});

describe("#formatDates", () => {
  it("formats date strings into a readable format", () => {
    const result = formatDates("01-02-2018", "01-03-2018");

    expect(result).toEqual("Jan 2, 2018 - Jan 3, 2018");
  });

  it("returns a single formatted date if both are the same", () => {
    const result = formatDates("01-02-2018", "01-02-2018");

    expect(result).toEqual("Jan 2, 2018");
  });
});
