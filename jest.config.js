module.exports = {
  moduleNameMapper: {
    "\\.css$": "<rootDir>/tests/__mocks__/styleMock.ts"
  },
  preset: "ts-jest",
  setupFiles: ["<rootDir>/jest.setup.js"]
};
